openshift.withCluster() {
  env.NAMESPACE = openshift.project()
  env.POM_FILE = env.BUILD_CONTEXT_DIR ? "${env.BUILD_CONTEXT_DIR}/pom.xml" : "pom.xml"
  env.APP_NAME = "${env.JOB_NAME}".replaceAll(/-?pipeline-?/, '').replaceAll(/-?${env.NAMESPACE}-?/, '').replaceAll("/", '')
  echo "Starting Pipeline for ${APP_NAME}..."
  def projectBase = "${env.NAMESPACE}".replaceAll(/-build/, '')
  env.STAGE0 = "${projectBase}-build"
  env.STAGE1 = "${projectBase}-dev"
  env.STAGE2 = "${projectBase}-stage"
  env.STAGE3 = "${projectBase}-prod"
}

pipeline {
  // Use Jenkins Maven slave
  // Jenkins will dynamically provision this as OpenShift Pod
  // All the stages and steps of this Pipeline will be executed on this Pod
  // After Pipeline completes the Pod is killed so every run will have clean
  // workspace
  agent {
    label 'maven'
    }

  // Pipeline Stages start here
  // Requeres at least one stage
  stages {

//    stage('Clean workspace') {
//      steps {
//        sh 'ls -lah'
//        deleteDir()
//        sh 'ls -lah'
//      }
//    }

    // Checkout source code
    // This is required as Pipeline code is originally checkedout to
    // Jenkins Master but this will also pull this same code to this slave
    stage('Git Checkout') {
      steps {
        echo "check"
        checkout scm
        // Turn off Git's SSL cert check, uncomment if needed
        // sh 'git config --global http.sslVerify false'
//        git url: "ssh://git@bitbucket.org/ag04/marvin.git"
      }
    }

    // Run Maven build, skipping tests
    stage('Build') {
      steps {
        sh './gradlew build --no-daemon --info --stacktrace'
      }
    }

//    // Run Maven unit tests
//    stage('Unit Test'){
//      steps {
//        sh "mvn test -f ${POM_FILE}"
//      }
//    }

//    stage('Create Image Builder') {
//      when {
//        expression {
//          openshift.withCluster() {
//            openshift.withProject("${STAGE0}") {
//              return !openshift.selector("bc", "${APP_NAME}").exists();
//            }
//          }
//        }
//      }
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.withProject("${STAGE0}") {
//            openshift.newBuild("--name=${APP_NAME}", "--image-stream=redhat-openjdk18-openshift:1.1", "--binary")
//          }
//        }
//      }
//    }

    // Build Container Image using the artifacts produced in previous stages
    stage('Build Container Image'){
      steps {
        // Copy the resulting artifacts into common directory
        sh """
          pwd
          ls -al * 
          rm -rf oc-build && mkdir -p oc-build/deployments
          cp build/libs/*.jar oc-build/deployments 2> /dev/null || echo "No \$t files"
        """

        // Build container image using local Openshift cluster
        // Giving all the artifacts to OpenShift Binary Build
        // This places your artifacts into right location inside your S2I image
        // if the S2I image supports it.
        script {
          openshift.withCluster() {

            openshift.withProject("${STAGE0}") {
//              openshift.verbose()
//              openshift.logLevel(5)
              openshift.selector("bc", "${APP_NAME}").startBuild("--from-dir=oc-build").logs("-f")
//            openshift.selector("bc", "${APP_NAME}").startBuild("--from-file=build/libs/XXXXX.jar", "--wait")
            }
          }
        }
      }
    }

    stage('Promote from Build to Dev') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("${env.STAGE0}/${env.APP_NAME}:latest", "${env.STAGE1}/${env.APP_NAME}:latest")
          }
        }
      }
    }

    stage ('Verify Deployment to Dev') {
      steps {
        script {
          openshift.withCluster() {
              openshift.withProject("${STAGE1}") {
              def dcObj = openshift.selector('dc', env.APP_NAME).object()
              def podSelector = openshift.selector('pod', [deployment: "${APP_NAME}-${dcObj.status.latestVersion}"])
              podSelector.untilEach {
                  echo "pod: ${it.name()}"
                  return it.object().status.containerStatuses[0].ready
              }
            }
          }
        }
      }
    }

    stage('Promote from Dev to Stage') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("${env.STAGE1}/${env.APP_NAME}:latest", "${env.STAGE2}/${env.APP_NAME}:latest")
          }
        }
      }
    }

    stage ('Verify Deployment to Stage') {
      steps {
        script {
          openshift.withCluster() {
              openshift.withProject("${STAGE2}") {
              def dcObj = openshift.selector('dc', env.APP_NAME).object()
              def podSelector = openshift.selector('pod', [deployment: "${APP_NAME}-${dcObj.status.latestVersion}"])
              podSelector.untilEach {
                  echo "pod: ${it.name()}"
                  return it.object().status.containerStatuses[0].ready
              }
            }
          }
        }
      }
    }
    stage('Promote from Stage to Prod') {
      steps {
        script {
          openshift.withCluster() {
            openshift.tag("${env.STAGE2}/${env.APP_NAME}:latest", "${env.STAGE3}/${env.APP_NAME}:latest")
          }
        }
      }
    }
    stage ('Verify Deployment to Prod') {
      steps {
        script {
          openshift.withCluster() {
              openshift.withProject("${STAGE3}") {
              def dcObj = openshift.selector('dc', env.APP_NAME).object()
              def podSelector = openshift.selector('pod', [deployment: "${APP_NAME}-${dcObj.status.latestVersion}"])
              podSelector.untilEach {
                  echo "pod: ${it.name()}"
                  return it.object().status.containerStatuses[0].ready
              }
            }
          }
        }
      }
    }

  }
}
// Ovo dole je bolje jer sam stvara bc dc i ostale image, ali treba prilagoditi
//
//pipeline {
//  agent {
//      label 'maven'
//  }
//  stages {
//    stage('Build App') {
//      steps {
//        sh "mvn install"
//      }
//    }
//    stage('Create Image Builder') {
//      when {
//        expression {
//          openshift.withCluster() {
//            return !openshift.selector("bc", "mapit").exists();
//          }
//        }
//      }
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.newBuild("--name=mapit", "--image-stream=redhat-openjdk18-openshift:1.1", "--binary")
//          }
//        }
//      }
//    }
//    stage('Build Image') {
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.selector("bc", "mapit").startBuild("--from-file=target/mapit-spring.jar", "--wait")
//          }
//        }
//      }
//    }
//    stage('Promote to DEV') {
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.tag("mapit:latest", "mapit:dev")
//          }
//        }
//      }
//    }
//    stage('Create DEV') {
//      when {
//        expression {
//          openshift.withCluster() {
//            return !openshift.selector('dc', 'mapit-dev').exists()
//          }
//        }
//      }
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.newApp("mapit:latest", "--name=mapit-dev").narrow('svc').expose()
//          }
//        }
//      }
//    }
//    stage('Promote STAGE') {
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.tag("mapit:dev", "mapit:stage")
//          }
//        }
//      }
//    }
//    stage('Create STAGE') {
//      when {
//        expression {
//          openshift.withCluster() {
//            return !openshift.selector('dc', 'mapit-stage').exists()
//          }
//        }
//      }
//      steps {
//        script {
//          openshift.withCluster() {
//            openshift.newApp("mapit:stage", "--name=mapit-stage").narrow('svc').expose()
//          }
//        }
//      }
//    }
//  }
//}
