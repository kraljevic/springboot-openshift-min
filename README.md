#Uputstva

Svi koraci nisu obavezni, ovisi o različitim stvarima koje su već instalirane.


## Projekt

oc create -f applier/projects/projects.yml 

###springboot-sti 

Sve što treba:

`oc create -f springboot-sti/springboot-sti-all.json -n marvin-build`

Ako je iz nekog razloga potrebno zasebno kreirati image streamove:

Zasebni image stream za centos:

`oc create  -f springboot-sti/centos-image-stream.json -n marvin-build`

Zasebni image stream za SPI:
`oc create  -f springboot-sti/springboot-sti-is.json -n marvin-build`


### Jenkins

Stvaranje Image streama

`oc create -f centos-jenkins-image-stream.json `

Jenkins-ephemeral template:

`oc create -f jenkins-ephemeral.json --save-config -n marvin-build`
`oc process marvin-build//jenkins-ephemeral --param-file jenkins-param   |oc apply -f-`

### Konfiguracija 
~~~
oc process -f deployment.yml --param-file params/deployment-dev | oc apply -f-

oc process -f deployment.yml --param-file params/deployment-stage | oc apply -f-

oc process -f deployment.yml --param-file params/deployment-prod | oc apply -f-

oc process -f build.yml --param-file params/build-dev | oc apply -f-

~~~

Nakon toga može se pokrenuti pipeline.

